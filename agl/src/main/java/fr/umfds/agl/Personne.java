package fr.umfds.agl;

public class Personne {
	private String nom;
	private String prenom;
	
	public Personne(String nom, String pre) {
		this.nom = nom;
		this.prenom = pre;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
