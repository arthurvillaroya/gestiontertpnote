package fr.umfds.agl;

public class Sujet {
	private String nom;
	private String description;
	
	public Sujet(String nom, String des) {
		this.nom = nom;
		this.description = des;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}

