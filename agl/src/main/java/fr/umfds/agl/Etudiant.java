package fr.umfds.agl;

import fr.umfds.agl.Personne;

public class Etudiant extends Personne {
	private String numEtu;
	
	public Etudiant(String num, String nom, String pre) {
		super(nom, pre);
		this.numEtu = num;
	}

	public String getNumEtu() {
		return numEtu;
	}
	public void setNumEtu(String numEtu) {
		this.numEtu = numEtu;
	}
}
