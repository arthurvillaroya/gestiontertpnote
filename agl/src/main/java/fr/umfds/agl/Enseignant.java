package fr.umfds.agl;

import fr.umfds.agl.Personne;
import fr.umfds.agl.Sujet;

public class Enseignant extends Personne{
	private String numEnseign;
	private Sujet sujet;
	
	public Enseignant(String num, Sujet suj, String nom, String pre) {
		super(nom, pre);
		this.numEnseign = num;
		this.sujet = suj;
	}
	
	public String getNumEseign() {
		return numEnseign;
	}
	public void setNumEseign(String numEnseign) {
		this.numEnseign = numEnseign;
	}
	
	public Sujet getSujet() {
		return sujet;
	}
	public void setSujet(Sujet sujet) {
		this.sujet = sujet;
	}
}
